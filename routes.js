import ContentsAPI from './apis/ContentsAPI';
import CommitsAPI from './apis/CommitsAPI';
import SearchAPI from './apis/SearchAPI';

const routes = (app) => {
    app.get('/contents', ContentsAPI.get);
    app.post('/contents', ContentsAPI.post);

    app.post('/commits', CommitsAPI.post);
    app.get('/commits', CommitsAPI.get);
    app.get('/commits/:commitId', CommitsAPI.getById);

    app.get('/search', SearchAPI.get);
};
export  default routes;


