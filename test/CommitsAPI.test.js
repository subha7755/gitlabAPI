import assert from 'assert';
import mockery from "mockery";
import fetch from 'isomorphic-fetch';
describe('CommitsAPI', () => {

    beforeEach(() => {
        mockery.enable({
            warnOnReplace: false,
            warnOnUnregistered: false,
            useCleanCache: true
        });
        mockery.registerMock("request-promise", () => Promise.resolve('Success'));
        require("./../index");
    });

    afterEach(() => {
        mockery.disable();
        mockery.deregisterAll();
    });

    it('should create a commit', () => {
        const actions = [
            {
                "action": "create",
                "file_path": "foo/bar",
                "content": "some content"
            },
            {
                "action": "delete",
                "file_path": "foo/bar2",
            },
            {
                "action": "move",
                "file_path": "foo/bar3",
                "previous_path": "foo/bar4",
                "content": "some content"
            },
            {
                "action": "update",
                "file_path": "foo/bar5",
                "content": "new content"
            }
        ];
        const data = {
            "commit_message": "Some commit mesasge",
            "author_email": "adsfd@sfdfh.com",
            "author_name": "adsfd",
            "actions": actions
        };
        return fetch('http://localhost:3000/commits', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(data)
        })
            .then((res) => assert.equal(res.status, 200))
    });
});
