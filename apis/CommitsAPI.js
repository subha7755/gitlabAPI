import request from "request-promise";

const CommitsAPI = {
    get: (req, res) => {
        const privateToken = process.env.PRIVATE_TOKEN;
        const projectId = process.env.PROJECT_ID;
        const authorName = req.query.authorName;
        const reqOptions = {
            method: 'GET',
            uri: `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
            headers: {"PRIVATE-TOKEN": privateToken},
            json: true
        };
        request(reqOptions)
            .then((response) => {
                const commitsByAuthor = response.filter(commit => commit.author_name.match(authorName));
                res.status(200).json(commitsByAuthor);
            })
            .catch((error) =>
                res.status(error.statusCode).json(error.error)
            );
    },

    getById: (req, res) => {
        const privateToken = process.env.PRIVATE_TOKEN;
        const projectId = process.env.PROJECT_ID;
        const commitId = req.params.commitId;
        const reqOptions = {
            method: 'GET',
            uri: `https://gitlab.com/api/v4/projects/${projectId}/repository/commits/${commitId}/diff`,
            headers: {"PRIVATE-TOKEN": privateToken},
            json: true
        };
        request(reqOptions)
            .then((response) => res.status(200).json(response))
            .catch((error) => res.status(error.statusCode).json(error.error));
    },

    post: (req, res) => {
        const privateToken = process.env.PRIVATE_TOKEN;
        const projectId = process.env.PROJECT_ID;
        const jsonPayLoad = {
            "branch": "master",
            "commit_message": req.body.commitMessage,
            "author_email": req.body.authorEmail,
            "author_name": req.body.authorName,
            "actions": req.body.actions
        };
        const reqOptions = {
            method: 'POST',
            uri: `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
            headers: {"PRIVATE-TOKEN": privateToken, "Content-Type": "application/json"},
            body: jsonPayLoad,
            json: true
        };
        request(reqOptions)
            .then(() => res.sendStatus(200))
            .catch((error) => res.status(error.statusCode).json(error.error));
    }
};

export default CommitsAPI;