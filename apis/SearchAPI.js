import request from "request-promise";

const SearchAPI = {

    get: (req, res) => {
        const privateToken = process.env.PRIVATE_TOKEN;
        const projectId = process.env.PROJECT_ID;
        const contentType = req.query.contentType;
        const reqOptions = {
            method: 'GET',
            uri: `https://gitlab.com/api/v4/projects/${projectId}/repository/tree?recursive=true`,
            headers: {"PRIVATE-TOKEN": privateToken},
            json: true
        };
        request(reqOptions)
            .then((response) => {
                const filesWithContentType = response.filter(file => file.name.match(`.${contentType}$`))
                    .map(file => file.path);
                res.status(200).json({files: filesWithContentType});
            })
            .catch((error) => res.status(error.statusCode).json(error.error));
    }
};

export default SearchAPI;