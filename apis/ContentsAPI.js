import request from "request-promise";

const ContentAPI = {
    get: (req, res) => {
        const privateToken = process.env.PRIVATE_TOKEN;
        const projectId = process.env.PROJECT_ID;
        const urlEncodedFilePath = encodeURIComponent(req.query.filePath);
        const reqOptions = {
            uri: `https://gitlab.com/api/v4/projects/${projectId}/repository/files/${urlEncodedFilePath}?ref=master`,
            headers: {"PRIVATE-TOKEN": privateToken},
            json: true
        };
        request(reqOptions)
            .then(({content}) => {
                const fileContents = Buffer.from(content, 'base64').toString();
                res.json(JSON.parse(fileContents));
            })
            .catch((error) => {
                if (!error.statusCode) throw new Error('Invalid JSON file');
                res.status(error.statusCode).json({message: 'Error in getting file'})
            })
            .catch((error) => res.status(422).json({messgae: error.message}));
    },

    post: (req, res) => {
        //There is a limit on the length of the URI so this will not support creating a file with large content
        const privateToken = process.env.PRIVATE_TOKEN;
        const projectId = process.env.PROJECT_ID;
        const urlEncodedFilePath = encodeURIComponent(req.query.filePath);
        const urlEncodedAuthorEmail = encodeURIComponent(req.body.authorEmail);
        const urlEncodedAuthorName = encodeURIComponent(req.body.authorName);
        const urlEncodedCommitMessage = encodeURIComponent(req.body.commitMessage);
        const urlEncodedContent = encodeURIComponent(JSON.stringify(req.body.content));
        const reqOptions = {
            method: 'POST',
            uri: `https://gitlab.com/api/v4/projects/${projectId}/repository/files/${urlEncodedFilePath}?branch=master&
            author_email=${urlEncodedAuthorEmail}&
            author_name=${urlEncodedAuthorName}&content=${urlEncodedContent}&commit_message=${urlEncodedCommitMessage}`,
            headers: {"PRIVATE-TOKEN": privateToken},
            json: true
        };
        request(reqOptions)
            .then(() => res.sendStatus(200))
            .catch((error) => res.status(error.statusCode).json(error.error));
    }
};

export default ContentAPI;
