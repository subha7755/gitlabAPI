import express from "express";
import bodyParser from "body-parser";
import routes from './routes.js';

const app = express();
app.use(bodyParser.json({limit: '4mb'}));

routes(app);
app.listen(3000, () => console.log(`Find the server at: http://localhost:3000/`));

